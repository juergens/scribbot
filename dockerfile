FROM ubuntu:bionic

###########################
### from ubuntu ###########
###########################

LABEL cache_breaker=2018_10_11

### python

RUN apt-get update && apt-get install -y \
	python-pip \
	python-configparser \
	libffi-dev \
	libssl-dev \
    python-dev \
    ffmpeg


RUN apt-get update && apt-get install -y git swig libpulse-dev

# pocketsphinx dep
RUN apt-get update && apt-get install -y python-dev bison autoconf libtool-bin swig libpulse-dev
RUN apt-get update && apt-get install -y python python-dev python-pip build-essential swig git libpulse-dev libasound2-dev


###########################
### python ################
###########################


WORKDIR /bot
COPY requirements.txt /bot
RUN pip install -r requirements.txt

COPY scribbot/ /bot

ENV PYTHONUNBUFFERED 0
CMD ["python", "__init__.py"]

# BUILD_DATE is set in .gitlab-ci.yml
# this is a cache-breaker to make sure the following commands
# don't get cached for too long
# ARG BUILD_DATE=unknown

# display the arg, so it shows up in the log in gitlab CI
# RUN echo $BUILD_DATE

# youtube-dl is volatile and requires fast updates
RUN pip install -U youtube-dl
