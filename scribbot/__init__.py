import hashlib
import time
import os

from reddit_base_bot import RedditBotBase
from reddit_base_bot.helper import l
import youtube_dl
import pyvtt
from bs4 import BeautifulSoup
from pastebin import PastebinAPI
import re

import subprocess
import traceback


class Bot(RedditBotBase):

    def __init__(self, reddit_client_id,
                 reddit_client_secret,
                 reddit_user,
                 reddit_password,
                 home_sub_name,
                 user_agent,
                 pastebin_user,
                 pastebin_password,
                 pastebin_dev_key):
        super(Bot, self).__init__(
            reddit_client_id=reddit_client_id,
            reddit_client_secret=reddit_client_secret,
            reddit_user=reddit_user,
            reddit_password=reddit_password,
            home_sub_name=home_sub_name,
            user_agent=user_agent)
        self.ffmpeg_full_path = "/usr/bin/ffmpeg"
        self.message_submission_name = "replies_from_stabbot"
        self.pastebin_dev_key = pastebin_dev_key
        self.pastebin = PastebinAPI()
        self.pastebin_user_key = self.pastebin.generate_user_key(self.pastebin_dev_key, pastebin_user,
                                                                 pastebin_password)
        l("pastebin details: " + unicode(self.pastebin.user_details(self.pastebin_dev_key, self.pastebin_user_key)))

    def handle_user_error(self, mention, text):
        """
        post on thread made by automod and ping summoner
        """
        text = "pinging /u/" + mention.author.name + "\n\n" + text
        if self.dry_run:
            l("message would be: " + text)
            return
        s = self.get_message_thread()
        l("replying...")
        s.reply(text)

    def subs(self, url, auto):
        # youtube-dl --write-sub https://www.youtube.com/watch\?v\=Ye8mB6VsUHw
        if auto:
            para = "writesubtitles"
        else:
            para = "writeautomaticsub"

        filename = u'download.vtt'
        ydl = youtube_dl.YoutubeDL({
            "outtmpl": filename,
            "quiet": True,
            para: True,
            "subtitleslangs": ['en'],
            "skip_download": True})
        ydl.download([url])
        for f in os.listdir("."):
            return self.parse_subs(f)

        pass

    def parse_subs(self, sub_file):
        result = ""
        vtt = pyvtt.open(sub_file)
        last_line = ""
        last_text = ""
        for cue in vtt:
            # text = cue.text
            # souped = BeautifulSoup(cue.text, "lxml").get_text()
            # add an extra line break, so it'll become a paragraph in markdown

            # if text == last_text:
            #    continue
            # for line in text.split("\n"):
            #    if line==last_line:
            #        continue
            #    result += text + "\n"
            #    last_line=line
            # result += "\n"
            if cue is None:
                continue

            result += cue.text + "\n\n"
        return result

    def subs_manu(self, url):
        return self.subs(url, False)

    def subs_auto(self, url):
        return self.subs(url, True)

    def scribbing(self, url):
        fs = [self.subs_auto]

        for f in fs:
            try:
                return f(url)
            except Exception as e:
                l("Exception:" + str(e.__class__) + str(e.__doc__) + str(e.message))
                l(e)
                traceback.print_exc()

    def process_summon(self, mention):
        l("submission: " + mention.submission.id + " - " + mention.submission.shortlink)
        start_time = time.time()
        l("scribbing...")
        text = self.scribbing(mention.submission.url)
        l("uploading...")
        paste_content = "subscripion of " + unicode(mention.submission.shortlink) + "\n\n\n" + unicode(text)
        p = self.pastebin.paste(
            api_dev_key=self.pastebin_dev_key,
            api_paste_code=paste_content,
            api_user_key=self.pastebin_user_key,
            paste_name='',
            paste_format=None,
            paste_private='public',
            paste_expire_date='1M')
        l("paste-result:")
        l(p)

        proc_time = time.time() - start_time

        return self.generate_text(text, proc_time)

    def generate_text(self, text, proc_time):

        result_text = "I have transcribed the video for you. \n " \
                      + "It took " + "%.f" % proc_time + " seconds to process:\n\n___\n\n" \
                      + text

        foot_text = "^^[&nbsp;how&nbsp;to&nbsp;use]" \
                    "(https://www.reddit.com/r/stabbot/comments/72irce/how_to_use_stabbot/)" \
                    "&nbsp;|&nbsp;[programmer](https://www.reddit.com/message/compose/?to=wotanii)" \
                    "&nbsp;|&nbsp;[source&nbsp;code](https://gitlab.com/juergens/scribbot)" \
                    "&nbsp;|&nbsp;/r/ImageStabilization/"

        return result_text \
               + "\n\n___\n\n" \
               + foot_text

    def get_message_thread(self):
        query = "title:\"" + self.message_submission_name + "\""

        candidates = self.home_sub.search(query, sort="new")
        return next(candidates)  # should be the first element


# ####################### #
# ## excecution ######### #
# ####################### #
reddit_client_id = os.environ['reddit_client_id']
reddit_client_secret = os.environ['reddit_client_secret']
reddit_user = os.environ['reddit_user']
reddit_password = os.environ['reddit_password']

pastebin_user = os.environ['pastebin_user']
pastebin_password = os.environ['pastebin_pw']
pastebin_dev_key = os.environ['pastebin_dev_key']

if __name__ == "__main__":
    b = Bot(reddit_client_id=reddit_client_id,
            reddit_client_secret=reddit_client_secret,
            reddit_user=reddit_user,
            reddit_password=reddit_password,
            home_sub_name="stabbot",
            user_agent="ubuntu:de.wotanii.scribbot:v0.1 (by /u/wotanii)",
            pastebin_user=pastebin_user,
            pastebin_password=pastebin_password,
            pastebin_dev_key=pastebin_dev_key)
    # b.clear_env()
    # s = 'https://www.youtube.com/watch?v=pnSQVixz7wg'
    # t = b.scribbing(s)
    # print(t)
    b()
